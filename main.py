to_search = [1, 2, 3, 4, 5, 6, 7]
to_find = 1


def binary_search(needle, haystack, low, high):
    if low <= high:
        mid = (low + high)//2
        if needle == haystack[mid]:
            return mid
        elif needle < haystack[mid]:
            return binary_search(needle, haystack, low, mid -1)
        else:
            return binary_search(needle, haystack, mid + 1, high)
    else:
        return -1


result = binary_search(to_find, to_search, 0, len(to_search)-1)


if result != -1:
    print("Item was found at index: " + str(result))
else:
    print("Item was not found")